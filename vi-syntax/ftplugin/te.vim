" Vim filetype plugin
" Language: TE (SELinux Type Enforcement Policy)
" Authors: Lukas Zapletal <lzap+git@redhat.com>, Christian Göttsche <cgzones@googlemail.com>
"
if exists("b:did_ftplugin_te")
    finish
endif

setlocal autoindent shiftwidth=8 softtabstop=8 noexpandtab
let b:did_ftplugin_te = 1

let s:plugin_path = expand("<sfile>:p:h:h")
