policy_module(unbound)

########################################
#
# Declarations
#

type unbound_t;
type unbound_exec_t;
systemd_service_domain(unbound_t, unbound_exec_t)
# control socket
rbac_public_system_unixsocket_object_exemption(unbound_t)

type unbound_anchor_exec_t;
corecmd_executable_file(unbound_anchor_exec_t)

type unbound_control_t;
type unbound_control_exec_t;
userdom_user_application_domain(unbound_control_t, unbound_control_exec_t)

type unbound_helper_t;
type unbound_helper_exec_t;
systemd_service_domain(unbound_helper_t, unbound_helper_exec_t)


type unbound_conf_t;
files_config_file(unbound_conf_t)

type unbound_initrc_exec_t;
systemd_service_initrc_file(unbound_initrc_exec_t)

type unbound_runtime_t;
files_runtime_file(unbound_runtime_t)

type unbound_state_t;
files_state_file(unbound_state_t)

type unbound_unit_t;
systemd_service_unit_file(unbound_unit_t)


########################################
#
# Policy for the unbound daemon
#

allow unbound_t self:capability { chown setgid setuid };
allow unbound_t self:process { getsched setrlimit };
allow unbound_t self:fifo_file { read write };
allow unbound_t self:netlink_route_socket { bind create getattr nlmsg_read read write };
allow unbound_t self:tcp_socket { accept bind connect create getopt listen read setopt write };
allow unbound_t self:udp_socket { bind connect create getopt read setopt write };
allow unbound_t self:unix_dgram_socket { connect create getopt setopt write };
allow unbound_t self:unix_stream_socket { accept bind connect create listen read write };


allow unbound_t unbound_conf_t:dir list_dir_perms;
allow unbound_t unbound_conf_t:file read_file_perms;

allow unbound_t unbound_runtime_t:sock_file { create setattr unlink };
files_runtime_filetrans(unbound_t, unbound_runtime_t, sock_file)

allow unbound_t unbound_state_t:dir rw_dir_perms;
allow unbound_t unbound_state_t:file manage_file_perms;


kernel_read_system_state(unbound_t)
kernel_read_public_kernel_sysctls(unbound_t)
kernel_read_vm_overcommit_sysctl(unbound_t)


corenet_send_stdextern_if(unbound_t)
corenet_send_lo_if(unbound_t)
corenet_send_generic_node(unbound_t)
corenet_send_lo_node(unbound_t)

corenet_tcp_bind_lo_node(unbound_t)
# constrained by firewall and access-control:
corenet_tcp_bind_anyaddr_node(unbound_t)
corenet_udp_bind_lo_node(unbound_t)
corenet_udp_bind_anyaddr_node(unbound_t)

corenet_tcp_bind_dns_port(unbound_t)
# 8953
corenet_tcp_bind_rndc_port(unbound_t)
corenet_udp_bind_dns_port(unbound_t)
corenet_udp_bind_rndc_port(unbound_t)
corenet_udp_bind_unreserved_ports(unbound_t)
# do not bind on free but designated ports
corenet_dontaudit_udp_bind_defined_ports(unbound_t)

corenet_tcp_connect_dns_port(unbound_t)

corenet_sendrecv_rndc_lo_server_packets(unbound_t)
corenet_sendrecv_dns_lo_server_packets(unbound_t)
corenet_sendrecv_dns_server_packets(unbound_t)
corenet_sendrecv_dns_client_packets(unbound_t)
# TODO: vpn secmark
corenet_send_ipsecnat_server_packets(unbound_t)

files_search_var_lib(unbound_t)

logging_send_syslog_msg(unbound_t)

miscfiles_read_config(unbound_t)
miscfiles_read_localization(unbound_t)

sysfs_read_cpu_possible(unbound_t)

systemd_use_notify(unbound_t)


########################################
#
# Policy for package-helper (started by systemd)
#

allow unbound_helper_t self:process getsched;
allow unbound_helper_t self:fifo_file { getattr read write };


allow unbound_helper_t unbound_conf_t:dir list_dir_perms;
allow unbound_helper_t unbound_conf_t:file read_file_perms;

allow unbound_helper_t unbound_state_t:dir search;
allow unbound_helper_t unbound_state_t:file getattr;


kernel_read_system_state(unbound_helper_t)
kernel_read_vm_overcommit_sysctl(unbound_helper_t)


corecmd_exec_bin(unbound_helper_t)
corecmd_exec_shell(unbound_helper_t)

# /usr/share/dns
files_read_usr_files(unbound_helper_t)
files_search_var_lib(unbound_helper_t)

miscfiles_read_localization(unbound_helper_t)

sysfs_read_cpu_possible(unbound_helper_t)


########################################
#
# Policy for unbound-control
#

allow unbound_control_t self:netlink_route_socket { bind create getattr nlmsg_read read write };
allow unbound_control_t self:tcp_socket { connect create getopt read write };
allow unbound_control_t self:unix_stream_socket { connect create getopt read write };

allow unbound_control_t unbound_t:unix_stream_socket connectto;

allow unbound_control_t unbound_conf_t:dir { getattr open read search };
allow unbound_control_t unbound_conf_t:file { getattr ioctl open read };

allow unbound_control_t unbound_runtime_t:sock_file write;


corenet_send_lo_if(unbound_control_t)
corenet_send_lo_node(unbound_control_t)

corenet_sendrecv_rndc_lo_client_packets(unbound_control_t)
corenet_tcp_connect_rndc_port(unbound_control_t)

domain_use_interactive_fds(unbound_control_t)

files_search_runtime(unbound_control_t)

miscfiles_read_config(unbound_control_t)
miscfiles_read_localization(unbound_control_t)

userdom_use_inherited_user_terminals(unbound_control_t)
