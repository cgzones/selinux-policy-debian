policy_module(rspamd)

########################################
#
# Declarations
#

type rspamadm_t;
type rspamadm_exec_t;
systemd_service_domain(rspamadm_t, rspamadm_exec_t)
application_domain(rspamadm_t, rspamadm_exec_t)

type rspamd_t;
type rspamd_exec_t;
systemd_service_domain(rspamd_t, rspamd_exec_t)

type rspamd_conf_t;
files_config_file(rspamd_conf_t)

type rspamd_initrc_exec_t;
systemd_service_initrc_file(rspamd_initrc_exec_t)

type rspamd_log_t;
logging_log_file(rspamd_log_t)

type rspamd_runtime_t;
files_runtime_file(rspamd_runtime_t)

type rspamd_shmfs_t;
files_shmfs_file(rspamd_shmfs_t)

type rspamd_state_t;
files_state_file(rspamd_state_t)

type rspamd_unit_t;
systemd_service_unit_file(rspamd_unit_t)


#######################################
#
# Rspamd policy
#

# execmem	: lua scripting
allow rspamd_t self:process { execmem getsched setrlimit sigkill signal signull };
allow rspamd_t self:fifo_file { read write };
allow rspamd_t self:tcp_socket { accept listen };
allow rspamd_t self:unix_stream_socket { bind connect create listen read setopt write };
allow rspamd_t self:unix_dgram_socket { connect create read sendto write };

allow rspamd_t rspamd_conf_t:dir { list_dir_perms watch };
allow rspamd_t rspamd_conf_t:file { mmap_read_file_perms watch };

allow rspamd_t rspamd_log_t:dir add_entry_dir_perms;
allow rspamd_t rspamd_log_t:file logging_file_perms;

allow rspamd_t rspamd_runtime_t:dir { add_name search write };
allow rspamd_t rspamd_runtime_t:file { create getattr lock open setattr write };

allow rspamd_t rspamd_state_t:dir { manage_dir_perms watch };
allow rspamd_t rspamd_state_t:file manage_file_perms;
allow rspamd_t rspamd_state_t:sock_file manage_sock_file_perms;

allow rspamd_t rspamd_shmfs_t:file manage_file_perms;
fs_shmfs_filetrans(rspamd_t, rspamd_shmfs_t, file)


kernel_read_vm_overcommit_sysctl(rspamd_t)
kernel_read_public_kernel_sysctls(rspamd_t)
# /proc/<pid>/net/dev
kernel_read_network_state(rspamd_t)


corenet_send_stdextern_if(rspamd_t)
corenet_send_generic_node(rspamd_t)

corenet_tcp_bind_anyaddr_node(rspamd_t)
corenet_tcp_bind_lo_node(rspamd_t)
corenet_tcp_bind_rspamd_port(rspamd_t)
corenet_tcp_bind_rspamdweb_port(rspamd_t)
corenet_tcp_connect_rspamd_port(rspamd_t)
corenet_tcp_connect_http_port(rspamd_t)
corenet_tcp_connect_redis_port(rspamd_t)

corenet_sendrecv_rspamd_lo_server_packets(rspamd_t)
corenet_sendrecv_rspamdweb_server_packets(rspamd_t)
corenet_sendrecv_http_client_packets(rspamd_t)
corenet_sendrecv_redis_lo_client_packets(rspamd_t)
# internal inter-process communication
corenet_sendrecv_rspamd_lo_client_packets(rspamd_t)
# fuzzy2.rspamd.com
corenet_sendrecv_rspamd_client_packets(rspamd_t)

dev_read_urand(rspamd_t)

# /usr/share/rspamd
files_mapread_usr_files(rspamd_t)
files_search_var_lib(rspamd_t)
files_search_runtime(rspamd_t)

fs_getattr_xattr_fs(rspamd_t)
fs_getattr_tmpfs(rspamd_t)

logging_search_logs(rspamd_t)
logging_send_syslog_msg(rspamd_t)

miscfiles_read_config(rspamd_t)
miscfiles_read_localization(rspamd_t)
miscfiles_read_generic_certs(rspamd_t)

sysfs_read_hugepage_info(rspamd_t)

sysnet_dns_name_resolve(rspamd_t)


########################################
#
# Rspamadm policy
#

# dac_override		: write /var/lib/rspamd
allow rspamadm_t self:capability { dac_override dac_read_search };
# type=PROCTITLE msg=audit(04/03/20 19:54:28.360:3944) : proctitle=rspamadm configtest
# type=SYSCALL msg=audit(04/03/20 19:54:28.360:3944) : arch=x86_64 syscall=unknown-syscall(425) success=yes exit=4 a0=0x20 a1=0x7ffdb1544dd0 a2=0x0 a3=0x7ffdb15ac080 items=0 ppid=1231 pid=1829552 auid=root uid=root gid=root euid=root suid=root fsuid=root egid=root sgid=root fsgid=root tty=pts1 ses=1 comm=rspamadm exe=/usr/bin/rspamadm subj=root:sysadm_r:rspamadm_t:s0-s0:c0.c1023 key=(null)
# type=AVC msg=audit(04/03/20 19:54:28.360:3944) : avc:  denied  { ipc_lock } for  pid=1829552 comm=rspamadm capability=ipc_lock  scontext=root:sysadm_r:rspamadm_t:s0-s0:c0.c1023 tcontext=root:sysadm_r:rspamadm_t:s0-s0:c0.c1023 tclass=capability permissive=0
dontaudit rspamadm_t self:capability ipc_lock;

# execmem	: lua scripting
allow rspamadm_t self:process { execmem getsched setrlimit signal };
allow rspamadm_t self:unix_dgram_socket { create };
allow rspamadm_t self:unix_stream_socket { connect create };

allow rspamadm_t rspamd_conf_t:dir list_dir_perms;
allow rspamadm_t rspamd_conf_t:file mmap_read_file_perms;

allow rspamadm_t rspamd_state_t:dir rw_dir_perms;
allow rspamadm_t rspamd_state_t:file manage_file_perms;


kernel_read_vm_overcommit_sysctl(rspamadm_t)
# /proc/<pid>/net/dev
kernel_read_network_state(rspamadm_t)


dev_read_urand(rspamadm_t)

domain_use_interactive_fds(rspamadm_t)

files_mapread_usr_files(rspamadm_t)
files_search_var_lib(rspamadm_t)

miscfiles_read_config(rspamadm_t)
miscfiles_read_localization(rspamadm_t)

sysfs_read_hugepage_info(rspamadm_t)

sysnet_dns_name_resolve(rspamadm_t)

userdom_use_inherited_user_terminals(rspamadm_t)
# chdir
userdom_search_user_home_content(rspamadm_t)
