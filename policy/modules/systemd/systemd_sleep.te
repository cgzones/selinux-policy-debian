policy_module(systemd_sleep)

########################################
#
# Declarations
#

type systemd_sleep_t;
type systemd_sleep_exec_t;
systemd_service_domain(systemd_sleep_t, systemd_sleep_exec_t)


type systemd_sleep_hook_t;
type systemd_sleep_hook_exec_t;
application_system_domain(systemd_sleep_hook_t, systemd_sleep_hook_exec_t)
corecmd_shell_entry_type(systemd_sleep_hook_t)


type systemd_sleep_conf_t;
files_config_file(systemd_sleep_conf_t)


########################################
#
# Policy for systemd-sleep
#

allow systemd_sleep_t self:capability sys_resource;
allow systemd_sleep_t self:process getcap;
allow systemd_sleep_t self:unix_dgram_socket { connect create getopt setopt write };
allow systemd_sleep_t self:unix_stream_socket { bind connect create getattr getopt read setopt write };

allow systemd_sleep_t systemd_sleep_conf_t:file read_file_perms;

allow systemd_sleep_t systemd_sleep_hook_exec_t:dir search_dir_perms;
domtrans_pattern(systemd_sleep_t, systemd_sleep_hook_exec_t, systemd_sleep_hook_t)


systemd_stream_connect(systemd_sleep_t)
systemd_read_state(systemd_sleep_t)
systemd_search_config(systemd_sleep_t)
systemd_startstop_user_slice(systemd_sleep_t)


kernel_read_system_state(systemd_sleep_t)
kernel_read_cmdline(systemd_sleep_t)
kernel_getattr_proc(systemd_sleep_t)
kernel_read_public_kernel_sysctls(systemd_sleep_t)


corecmd_exec_shell(systemd_sleep_t)

fs_getattr_cgroup(systemd_sleep_t)
fs_search_cgroup_dirs(systemd_sleep_t)

logging_send_syslog_msg(systemd_sleep_t)

selinux_getattr_fs(systemd_sleep_t)

seutil_search_config(systemd_sleep_t)

sysfs_write_power(systemd_sleep_t)


optional_policy(`
	dbus_system_bus_client(systemd_sleep_t)

	systemd_dbus_chat(systemd_sleep_t)
')


########################################
#
# Policy for systemd-sleep hooks
#

# sys_rawio	: hdparm ioctl SG_IO on /dev/sda
allow systemd_sleep_hook_t self:capability sys_rawio;
allow systemd_sleep_hook_t self:fifo_file { getattr ioctl read write };
allow systemd_sleep_hook_t self:unix_stream_socket { bind connect create getattr getopt read setopt write };


systemd_read_state(systemd_sleep_hook_t)
systemd_use_fds(systemd_sleep_hook_t)
systemd_stream_connect(systemd_sleep_hook_t)
systemd_rw_inherited_stream_sockets(systemd_sleep_hook_t)


kernel_read_cmdline(systemd_sleep_hook_t)


# systemctl
corecmd_exec_bin(systemd_sleep_hook_t)

fstools_exec(systemd_sleep_hook_t)
fstools_read_config(systemd_sleep_hook_t)

miscfiles_read_config(systemd_sleep_hook_t)
miscfiles_read_localization(systemd_sleep_hook_t)

selinux_getattr_fs(systemd_sleep_hook_t)
selinux_map_status(systemd_sleep_hook_t)

seutil_read_config(systemd_sleep_hook_t)
seutil_read_file_contexts(systemd_sleep_hook_t)

storage_raw_read_fixed_disk_dev(systemd_sleep_hook_t)
storage_raw_control_fixed_disk_dev(systemd_sleep_hook_t, { ioctl_sg_io })

sysfs_read_harddrive(systemd_sleep_hook_t)
sysfs_read_virtual_dev(systemd_sleep_hook_t)
sysfs_read_generic_device(systemd_sleep_hook_t)
sysfs_list_generic(systemd_sleep_hook_t)

udev_exec(systemd_sleep_hook_t)
udev_read_config(systemd_sleep_hook_t)
udev_read_runtime_files(systemd_sleep_hook_t)


optional_policy(`
	fancontrol_start(systemd_sleep_hook_t)
	fancontrol_status(systemd_sleep_hook_t)
')
