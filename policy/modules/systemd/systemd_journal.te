policy_module(systemd_journal)

gen_require(`
	class service { stop };
')

########################################
#
# Declarations
#

attribute_role systemd_journal_ctl_roles;


type systemd_journal_ctl_t;
type systemd_journal_ctl_exec_t;
systemd_service_domain(systemd_journal_ctl_t, systemd_journal_ctl_exec_t)
role systemd_journal_ctl_roles types systemd_journal_ctl_t;

type systemd_journald_t;
type systemd_journald_exec_t;
systemd_service_domain(systemd_journald_t, systemd_journald_exec_t)
# send signull to user processes
rbac_process_subject_read_exemption(systemd_journald_t)
# sink for syslog from all roles
rbac_public_system_unixsocket_object_exemption(systemd_journald_t)

type systemd_journal_catalog_t;
files_state_file(systemd_journal_catalog_t)

# journalctl --verify
type systemd_journal_ctl_tmp_t;
files_tmp_file(systemd_journal_ctl_tmp_t)

type systemd_journal_data_t;
files_state_file(systemd_journal_data_t)
fs_associate_tmpfs(systemd_journal_data_t) # created on tmpfs in early boot stage

type systemd_journald_conf_t;
files_config_file(systemd_journald_conf_t)

type systemd_journald_logsink_t;
files_runtime_file(systemd_journald_logsink_t)

type systemd_journald_runtime_t;
files_runtime_file(systemd_journald_runtime_t)

type systemd_journald_unit_t;
systemd_service_unit_file(systemd_journald_unit_t)


########################################
#
# journalctl policy
#

allow systemd_journal_ctl_t self:capability { sys_ptrace sys_resource };
allow systemd_journal_ctl_t self:process { getcap setrlimit signal };
allow systemd_journal_ctl_t self:fifo_file { getattr ioctl read write };
allow systemd_journal_ctl_t self:unix_dgram_socket { connect create getopt setopt };
allow systemd_journal_ctl_t self:unix_stream_socket { connect create getattr getopt read setopt write };

allow systemd_journal_ctl_t systemd_journal_catalog_t:dir rw_dir_perms;
allow systemd_journal_ctl_t systemd_journal_catalog_t:file manage_file_perms;

allow systemd_journal_ctl_t systemd_journal_ctl_tmp_t:file manage_file_perms;
files_tmp_filetrans(systemd_journal_ctl_t, systemd_journal_ctl_tmp_t, file)

allow systemd_journal_ctl_t systemd_journal_data_t:dir { list_dir_perms watch };
allow systemd_journal_ctl_t systemd_journal_data_t:file mmap_read_file_perms;

allow systemd_journal_ctl_t systemd_journald_logsink_t:sock_file write;

allow systemd_journal_ctl_t systemd_journald_runtime_t:dir list_dir_perms;
allow systemd_journal_ctl_t systemd_journald_runtime_t:sock_file write;

allow systemd_journal_ctl_t systemd_journald_t:unix_dgram_socket sendto;
allow systemd_journal_ctl_t systemd_journald_t:unix_stream_socket connectto;

allow systemd_journal_ctl_t systemd_journald_unit_t:service stop;

systemd_read_state(systemd_journal_ctl_t)
systemd_read_machine_id(systemd_journal_ctl_t)
systemd_stream_connect(systemd_journal_ctl_t)
systemd_search_var_lib(systemd_journal_ctl_t)


kernel_read_public_fs_sysctls(systemd_journal_ctl_t)
kernel_read_public_kernel_sysctls(systemd_journal_ctl_t)
kernel_read_system_state(systemd_journal_ctl_t)
kernel_read_cmdline(systemd_journal_ctl_t)
kernel_getattr_proc(systemd_journal_ctl_t)


corecmd_exec_bin(systemd_journal_ctl_t)

domain_use_interactive_fds(systemd_journal_ctl_t)

files_search_etc(systemd_journal_ctl_t)
files_search_runtime(systemd_journal_ctl_t)

# /run
fs_getattr_tmpfs(systemd_journal_ctl_t)
fs_getattr_xattr_fs(systemd_journal_ctl_t)
fs_getattr_cgroup(systemd_journal_ctl_t)
fs_search_cgroup_dirs(systemd_journal_ctl_t)
fs_getattr_nsfs_files(systemd_journal_ctl_t)

logging_search_logs(systemd_journal_ctl_t)

miscfiles_read_config(systemd_journal_ctl_t)
miscfiles_read_localization(systemd_journal_ctl_t)
miscfiles_read_terminfo(systemd_journal_ctl_t)

selinux_getattr_fs(systemd_journal_ctl_t)

seutil_search_config(systemd_journal_ctl_t)

sysfs_search_firmware(systemd_journal_ctl_t)

term_drain_controlling_term(systemd_journal_ctl_t)
# less (as pager implementation)
term_search_ptys(systemd_journal_ctl_t)

userdom_use_inherited_user_terminals(systemd_journal_ctl_t)
userdom_dontaudit_search_user_home_content(systemd_journal_ctl_t)
userdom_dontaudit_open_user_terminals(systemd_journal_ctl_t)
userdom_search_user_config_root(systemd_journal_ctl_t)
userdom_manage_user_less_home_files(systemd_journal_ctl_t)


optional_policy(`
	# called by dpkg trigger
	apt_use_ptys(systemd_journal_ctl_t)
')


########################################
#
# systemd-journald policy
#

allow systemd_journald_t self:capability { dac_read_search setgid setuid sys_admin sys_ptrace };
allow systemd_journald_t self:cap_userns { kill sys_ptrace };
allow systemd_journald_t self:process signal;
allow systemd_journald_t self:unix_dgram_socket { connect create getattr getopt read setopt write };
allow systemd_journald_t self:unix_stream_socket { accept bind create getattr getopt listen read setopt shutdown write };


allow systemd_journald_t systemd_journal_data_t:dir manage_dir_perms;
allow systemd_journald_t systemd_journal_data_t:file manage_file_perms;

allow systemd_journald_t systemd_journald_conf_t:file read_file_perms;

allow systemd_journald_t systemd_journald_logsink_t:sock_file write;

allow systemd_journald_t systemd_journald_runtime_t:dir manage_dir_perms;
allow systemd_journald_t systemd_journald_runtime_t:file manage_file_perms;
allow systemd_journald_t systemd_journald_runtime_t:sock_file manage_sock_file_perms;

systemd_search_config(systemd_journald_t)
systemd_read_machine_id(systemd_journald_t)
systemd_dgram_send(systemd_journald_t)
systemd_use_notify(systemd_journald_t)
systemd_getattr(systemd_journald_t)
systemd_read_runtime_unit_symlinks(systemd_journald_t)

# invocations
systemd_user_instance_read_runtime_symlinks(systemd_journald_t)


kernel_read_ring_buffer(systemd_journald_t)
kernel_read_system_state(systemd_journald_t)
kernel_read_cmdline(systemd_journald_t)
kernel_getattr_proc(systemd_journald_t)
kernel_read_public_kernel_sysctls(systemd_journald_t)


auth_read_utmp(systemd_journald_t)

dev_read_kmsg(systemd_journald_t)
dev_write_kmsg(systemd_journald_t)

domain_read_all_domains_state(systemd_journald_t)
domain_getattr_all_domains(systemd_journald_t)
domain_signull_all_domains(systemd_journald_t)

fs_getattr_cgroup(systemd_journald_t)
fs_list_cgroup_all_dirs(systemd_journald_t)
# TODO: /sys/fs/cgroup/system.slice/systemd-logind.service/memory.pressure
fs_read_cgroup_systemslice_files(systemd_journald_t)
fs_dontaudit_write_cgroup_systemslice_files(systemd_journald_t)
fs_getattr_nsfs_files(systemd_journald_t)
fs_getattr_tmpfs(systemd_journald_t)
fs_getattr_xattr_fs(systemd_journald_t)

logging_search_logs(systemd_journald_t)
logging_runtime_filetrans_generic(systemd_journald_t, dir, "log")
logging_log_filetrans(systemd_journald_t, systemd_journal_data_t, dir, "journal")
logging_create_generic_log_dirs(systemd_journald_t)
# /run/log/journal/
logging_delete_generic_log_dir_entries(systemd_journald_t)
# Audit= option
logging_toggle_audit(systemd_journald_t)

miscfiles_read_localization(systemd_journald_t)

selinux_getattr_fs(systemd_journald_t)

seutil_search_config(systemd_journald_t)
seutil_search_setrans_runtime(systemd_journald_t)

sysfs_read_generic_device(systemd_journald_t)
sysfs_search_firmware(systemd_journald_t)
# /sys/module/printk/parameters/time
sysfs_read_modules(systemd_journald_t)
# /sys/devices/system/cpu/cpu0/uevent
sysfs_read_cpu(systemd_journald_t)

# late shutdown
term_write_console(systemd_journald_t)
term_control_console(systemd_journald_t, ioctl_tcgets)
# log to serial console
term_write_unallocated_ttys(systemd_journald_t)

udev_read_runtime_files(systemd_journald_t)
