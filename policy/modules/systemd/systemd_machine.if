## <summary>systemd-machine -- Virtual machine and container registration manager.</summary>
## <required val="true" />

########################################
## <summary>
##	Talk to systemd-machine userdbctl interface.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_machine_use_userdb',`
	gen_require(`
		type systemd_machine_userdb_t, systemd_machined_t;
	')

	allow $1 systemd_machine_userdb_t:sock_file write;
	allow $1 systemd_machined_t:unix_stream_socket connectto;
')

########################################
## <summary>
##	Send and receive messages from
##	systemd-machine over dbus.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_machine_dbus_chat',`
	gen_require(`
		type systemd_machined_t;
	')

	DBus_send_pattern($1, systemd_machined_t)
')

########################################
## <summary>
##	Watch systemd-machined runtime directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_machine_watch_runtime_dirs',`
	gen_require(`
		type systemd_machine_runtime_t;
	')

	allow $1 systemd_machine_runtime_t:dir watch_dir_perms;
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`systemd_machine__systemd',`
	systemd_machine_dbus_chat($1)
')

########################################
## <summary>
##	Reverse interface for systemd-tmpfiles.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`systemd_machine__systemdtmpfiles',`
	gen_require(`
		type systemd_machine_runtime_t;
	')

	allow $1 systemd_machine_runtime_t:dir { create getattr open read relabelfrom relabelto };
')

########################################
## <summary>
##	All of the rules required to
##	administrate a systemd-machine environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`systemd_machine__admin',`
	gen_require(`
		type systemd_machined_t;
	')

	systemd_machine_use_userdb($1)
	systemd_machine_dbus_chat($1)

	allow $1 systemd_machined_t:fd use;
')
