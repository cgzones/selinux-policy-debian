#
# sysfs_detail(name, path, [,dependencies [,...]])
#
define(`sysfs_detail',`
type $1_sysfs_t, sysfs_type;
files_type($1_sysfs_t)
sysfs_associate($1_sysfs_t)
sysfs_fscon($1, $2)dnl
')

#
# sysfs_fscon(name, path)
#
define(`sysfs_fscon',`ifelse($2,none,,genfscon sysfs $2 gen_context(system_u:object_r:$1_sysfs_t,s0))')
