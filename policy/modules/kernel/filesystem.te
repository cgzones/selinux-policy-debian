policy_module(filesystem)

########################################
#
# Declarations
#

# all filesystems
attribute filesystem_type;
# filesystems supporting extended attributes
attribute xattrfs;
# filesystems supporting extended attributes,
# but not modifiable from userspace
attribute genfskernfs;
# filesystems not supporting extended attributes
attribute noxattrfs;
# cgroup filesystem entry
attribute cgroup_type;


##############################
#
# fs_t is the default type for persistent
# filesystems with extended attributes
#
type fs_t;
fs_xattr_type(fs_t)

# Use xattrs for the following filesystem types.
# Requires that a security xattr handler exist for the filesystem.
fs_use_xattr btrfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr encfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr erofs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr ext2 gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr ext3 gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr ext4 gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr ext4dev gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr f2fs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr gfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr gfs2 gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr gpfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr jffs2 gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr jfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr lustre gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr overlay gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr squashfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr ubifs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr xfs gen_context(system_u:object_r:fs_t,s0);
fs_use_xattr zfs gen_context(system_u:object_r:fs_t,s0);

# Use the allocating task SID to label inodes in the following filesystem
# types, and label the filesystem itself with the specified context.
# This is appropriate for pseudo filesystems that represent objects
# like pipes and sockets, so that these objects are labeled with the same
# type as the creating task.
type pseudo_fs_t;
fs_type(pseudo_fs_t)

fs_use_task eventpollfs gen_context(system_u:object_r:pseudo_fs_t,s0);
fs_use_task pipefs gen_context(system_u:object_r:pseudo_fs_t,s0);
fs_use_task sockfs gen_context(system_u:object_r:pseudo_fs_t,s0);
fs_use_task pidfs gen_context(system_u:object_r:pseudo_fs_t,s0);


##############################
#
# Non-persistent/pseudo filesystems
#

type anon_inodefs_t;
fs_type(anon_inodefs_t)
genfscon anon_inodefs / gen_context(system_u:object_r:anon_inodefs_t,s0)

type bdev_t;
fs_type(bdev_t)
genfscon bdev / gen_context(system_u:object_r:bdev_t,s0)

type binfmt_misc_fs_t;
fs_type(binfmt_misc_fs_t)
genfscon binfmt_misc / gen_context(system_u:object_r:binfmt_misc_fs_t,s0)

type bpf_t;
fs_xattr_type(bpf_t)
sysfs_associate(bpf_t)
genfscon bpf / gen_context(system_u:object_r:bpf_t,s0)

type capifs_t;
fs_type(capifs_t)
genfscon capifs / gen_context(system_u:object_r:capifs_t,s0)

type cgroup_t, cgroup_type;
type cgroup_system_slice_t, cgroup_type;
type cgroup_user_slice_t, cgroup_type;
allow cgroup_type cgroup_t:filesystem associate;
fs_xattr_type(cgroup_t)
sysfs_associate(cgroup_t) # /sys/fs/cgroup
genfscon cgroup / gen_context(system_u:object_r:cgroup_t,s0)
genfscon cgroup2 / gen_context(system_u:object_r:cgroup_t,s0)
genfscon cgroup2 "/system.slice" -d gen_context(system_u:object_r:cgroup_system_slice_t,s0)
genfscon cgroup2 "/user.slice" gen_context(system_u:object_r:cgroup_user_slice_t,s0)

type configfs_t;
fs_type(configfs_t)
genfscon configfs / gen_context(system_u:object_r:configfs_t,s0)

type cpusetfs_t;
fs_type(cpusetfs_t)
genfscon cpuset / gen_context(system_u:object_r:cpusetfs_t,s0)

type ecryptfs_t;
fs_noxattr_type(ecryptfs_t)
genfscon ecryptfs / gen_context(system_u:object_r:ecryptfs_t,s0)

type efivarfs_t;
fs_noxattr_type(efivarfs_t)
genfscon efivarfs / gen_context(system_u:object_r:efivarfs_t,s0)

type futexfs_t;
fs_type(futexfs_t)
genfscon futexfs / gen_context(system_u:object_r:futexfs_t,s0)

type hugetlbfs_t;
fs_xattr_type(hugetlbfs_t)
dev_associate(hugetlbfs_t) # /dev/hugepages
fs_use_trans hugetlbfs gen_context(system_u:object_r:hugetlbfs_t,s0);

type ibmasmfs_t;
fs_type(ibmasmfs_t)
genfscon ibmasmfs / gen_context(system_u:object_r:ibmasmfs_t,s0)

type infinibandeventfs_t;
fs_type(infinibandeventfs_t)
genfscon infinibandeventfs / gen_context(system_u:object_r:infinibandeventfs_t,s0)

type inotifyfs_t;
fs_type(inotifyfs_t)
genfscon inotifyfs / gen_context(system_u:object_r:inotifyfs_t,s0)

type mvfs_t;
fs_noxattr_type(mvfs_t)
genfscon mvfs / gen_context(system_u:object_r:mvfs_t,s0)

type nfsd_fs_t;
fs_type(nfsd_fs_t)
genfscon nfsd / gen_context(system_u:object_r:nfsd_fs_t,s0)

type nsfs_t;
fs_type(nsfs_t)
genfscon nsfs / gen_context(system_u:object_r:nsfs_t,s0)

type oprofilefs_t;
fs_type(oprofilefs_t)
genfscon oprofilefs / gen_context(system_u:object_r:oprofilefs_t,s0)

type pstore_t;
fs_xattr_type(pstore_t)
sysfs_associate(pstore_t) # /sys/fs/pstore
genfscon pstore / gen_context(system_u:object_r:pstore_t,s0)

type ramfs_t;
fs_xattr_type(ramfs_t)
fs_use_trans ramfs gen_context(system_u:object_r:ramfs_t,s0);

type romfs_t;
fs_type(romfs_t)
genfscon romfs / gen_context(system_u:object_r:romfs_t,s0)
genfscon cramfs / gen_context(system_u:object_r:romfs_t,s0)

type rpc_pipefs_t;
fs_type(rpc_pipefs_t)
genfscon rpc_pipefs / gen_context(system_u:object_r:rpc_pipefs_t,s0)

type spufs_t;
fs_type(spufs_t)
genfscon spufs / gen_context(system_u:object_r:spufs_t,s0)

type sysv_t;
fs_noxattr_type(sysv_t)
genfscon sysv / gen_context(system_u:object_r:sysv_t,s0)
genfscon v7 / gen_context(system_u:object_r:sysv_t,s0)

type tracefs_t;
fs_xattr_type(tracefs_t)
genfscon tracefs / gen_context(system_u:object_r:tracefs_t,s0)
genfscon debugfs /tracing gen_context(system_u:object_r:tracefs_t,s0)

type vmblock_t;
fs_noxattr_type(vmblock_t)
genfscon vmblock / gen_context(system_u:object_r:vmblock_t,s0)
genfscon vboxsf / gen_context(system_u:object_r:vmblock_t,s0)
genfscon vmhgfs / gen_context(system_u:object_r:vmblock_t,s0)

type vxfs_t;
fs_noxattr_type(vxfs_t)
genfscon vxfs / gen_context(system_u:object_r:vxfs_t,s0)

type xenfs_t;
fs_noxattr_type(xenfs_t)
genfscon xenfs / gen_context(system_u:object_r:xenfs_t,s0)


########################################
#
# Temporary filesystems like tmpfs
#

# Use a transition SID based on the allocating task SID and the
# filesystem SID to label inodes in the following filesystem types,
# and label the filesystem itself with the specified context.
# This is appropriate for pseudo filesystems like devpts and tmpfs
# where we want to label objects with a derived type.

type mqueuefs_t;
fs_xattr_type(mqueuefs_t)
dev_associate(mqueuefs_t) # /dev/mqueue
fs_use_trans mqueue gen_context(system_u:object_r:mqueuefs_t,s0);

type shmfs_t;
fs_xattr_type(shmfs_t)
dev_associate(shmfs_t) # /dev/shm
fs_associate_tmpfs(shmfs_t) # shm is mounted as a tmpfs filesystem
#genfscon shm / gen_context(system_u:object_r:shmfs_t,s0)
fs_use_trans shm gen_context(system_u:object_r:shmfs_t,s0);

type tmpfs_t;
fs_xattr_type(tmpfs_t)
fs_use_trans tmpfs gen_context(system_u:object_r:tmpfs_t,s0);

# clients should use private types
neverallow * tmpfs_t:dir ~{ add_name list_dir_perms mounton relabelfrom remove_name write };
neverallow * tmpfs_t:lnk_file ~{ read_lnk_file_perms relabelfrom };
neverallow * tmpfs_t:file ~{ getattr relabelfrom };
neverallow * tmpfs_t:fifo_file *;
neverallow * tmpfs_t:blk_file *;
neverallow * tmpfs_t:chr_file *;
neverallow * tmpfs_t:sock_file ~{ getattr relabelfrom };


##############################
#
# Filesystems without extended attribute support
#

type autofs_t;
fs_noxattr_type(autofs_t)
genfscon autofs / gen_context(system_u:object_r:autofs_t,s0)
genfscon automount / gen_context(system_u:object_r:autofs_t,s0)

#
# cifs_t is the type for filesystems and their
# files shared from Windows servers
#
type cifs_t;
fs_noxattr_type(cifs_t)
genfscon cifs / gen_context(system_u:object_r:cifs_t,s0)
genfscon smbfs / gen_context(system_u:object_r:cifs_t,s0)

#
# dosfs_t is the type for fat and vfat
# filesystems and their files.
#
type dosfs_t;
fs_noxattr_type(dosfs_t)
#allow dosfs_t fs_t:filesystem associate;
genfscon fat / gen_context(system_u:object_r:dosfs_t,s0)
genfscon hfs / gen_context(system_u:object_r:dosfs_t,s0)
genfscon hfsplus / gen_context(system_u:object_r:dosfs_t,s0)
genfscon msdos / gen_context(system_u:object_r:dosfs_t,s0)
genfscon ntfs-3g / gen_context(system_u:object_r:dosfs_t,s0)
genfscon ntfs3 / gen_context(system_u:object_r:dosfs_t,s0)
genfscon ntfs / gen_context(system_u:object_r:dosfs_t,s0)
genfscon vfat / gen_context(system_u:object_r:dosfs_t,s0)
genfscon exfat / gen_context(system_u:object_r:dosfs_t,s0)

type fusefs_t;
fs_noxattr_type(fusefs_t)
genfscon fuse / gen_context(system_u:object_r:fusefs_t,s0)
genfscon fuseblk / gen_context(system_u:object_r:fusefs_t,s0)

type fusectl_t;
fs_type(fusectl_t)
genfscon fusectl / gen_context(system_u:object_r:fusectl_t,s0)

#
# iso9660_t is the type for CD filesystems
# and their files.
#
type iso9660_t;
fs_noxattr_type(iso9660_t)
genfscon iso9660 / gen_context(system_u:object_r:iso9660_t,s0)
genfscon udf / gen_context(system_u:object_r:iso9660_t,s0)

#
# removable_t is the default type of all removable media
#
type removable_t;
fs_noxattr_type(removable_t)

#
# nfs_t is the default type for NFS file systems
# and their files.
#
type nfs_t;
fs_noxattr_type(nfs_t)
genfscon nfs / gen_context(system_u:object_r:nfs_t,s0)
genfscon nfs4 / gen_context(system_u:object_r:nfs_t,s0)
genfscon afs / gen_context(system_u:object_r:nfs_t,s0)
genfscon dazukofs / gen_context(system_u:object_r:nfs_t,s0)
genfscon coda / gen_context(system_u:object_r:nfs_t,s0)
genfscon lustre / gen_context(system_u:object_r:nfs_t,s0)
genfscon ncpfs / gen_context(system_u:object_r:nfs_t,s0)
genfscon reiserfs / gen_context(system_u:object_r:nfs_t,s0)
genfscon panfs / gen_context(system_u:object_r:nfs_t,s0)


########################################
#
# Rules for filesystems with extended attribute support
#

allow xattrfs self:filesystem associate;
neverallow xattrfs ~xattrfs:filesystem associate;
neverallow { filesystem_type -xattrfs } xattrfs:filesystem associate;

########################################
#
# Rules for filesystems without extended attribute support
#

allow noxattrfs self:filesystem associate; # creating files on noxattr filesystems

neverallow * noxattrfs:file entrypoint;
neverallow * noxattrfs:{ dir_file_class_set anon_inode } relabelto;
neverallow * { noxattrfs -autofs_t }:{ dir_file_class_set anon_inode } mounton;
neverallow ~noxattrfs noxattrfs:filesystem associate;
neverallow noxattrfs ~noxattrfs:filesystem associate;
neverallow noxattrfs ~self:filesystem associate;

########################################
#
# Rules for kernel filesystem types
#

neverallow * genfskernfs:dir ~{ getattr ioctl lock mounton open read search watch };
neverallow * genfskernfs:file_class_set ~{ append getattr ioctl lock map mounton open read write };
neverallow ~container_manager_domain genfskernfs:file_class_set ~{ append getattr ioctl lock map open read write }; #selint-disable: W-001
neverallow genfskernfs *:filesystem associate;
neverallow ~dynfddomain genfskernfs:filesystem associate; #selint-disable: W-001

########################################
#
# Rules for volatile filesystem types
#

neverallow * hugetlbfs_t:dir ~{ create list_dir_perms mounton };
neverallow * hugetlbfs_t:file ~{ getattr map read write };
neverallow * hugetlbfs_t:lnk_file ~{ getattr };
neverallow * hugetlbfs_t:fifo_file *;
neverallow * hugetlbfs_t:blk_file *;
neverallow * hugetlbfs_t:chr_file *;
neverallow * hugetlbfs_t:sock_file *;

neverallow tmpfs_t ~tmpfs_t:filesystem associate;
