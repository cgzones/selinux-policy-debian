## <summary>Personal package builder for Debian packages.</summary>

########################################
## <summary>
##	Execute the pbuilder program in the pbuilder domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`pbuilder_domtrans',`
	gen_require(`
		type pbuilder_t, pbuilder_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, pbuilder_exec_t, pbuilder_t)
')

########################################
## <summary>
##	Execute the pbuilder program in the pbuilder domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`pbuilder_run',`
	gen_require(`
		type pbuilder_t, pbuilder_iouring_t;
	')

	pbuilder_domtrans($1)
	role $2 types { pbuilder_t pbuilder_iouring_t };
')

########################################
## <summary>
##	Search the pbuilder cache directories.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`pbuilder_search_cache',`
	gen_require(`
		type pbuilder_cache_t;
	')

	files_search_var_cache($1)
	allow $1 pbuilder_cache_t:dir search_dir_perms;
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`pbuilder__systemd',`
	gen_require(`
		type pbuilder_t, pbuilder_exec_t;
	')

	# pidfd
	allow $1 pbuilder_t:fd use;

	allow $1 pbuilder_exec_t:file getattr;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an pbuilder environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`pbuilder__admin',`
	gen_require(`
		type pbuilder_cache_t;
	')

	pbuilder_run($1, $2)

	delete_chr_files_pattern($1, pbuilder_cache_t, pbuilder_cache_t)
')
