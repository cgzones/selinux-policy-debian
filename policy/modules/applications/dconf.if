## <summary> dconf - simple configuration storage system - D-Bus service.</summary>

########################################
## <summary>
##	Role access for dconf.
## </summary>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	User domain for the role.
##	</summary>
## </param>
#
interface(`dconf_role',`
	gen_require(`
		type dconf_t, dconf_user_runtime_t;
	')

	role $1 types dconf_t;

	allow $2 dconf_user_runtime_t:dir add_entry_dir_perms;
	allow $2 dconf_user_runtime_t:file { create mmap_rw_file_perms };

	optional_policy(`
		dconf_dbus_chat($2)
	')
')

########################################
## <summary>
##	Send and receive messages from
##	dconf over dbus.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dconf_dbus_chat',`
	gen_require(`
		type dconf_t;
	')

	DBus_send_pattern($1, dconf_t)
	# dconf chats back
	DBus_send_pattern(dconf_t, $1)
')
