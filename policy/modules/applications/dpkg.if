## <summary>Debian package manager.</summary>

########################################
## <summary>
##	Execute dpkg programs in the dpkg domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dpkg_domtrans',`
	gen_require(`
		type dpkg_t, dpkg_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, dpkg_exec_t, dpkg_t)
')

########################################
## <summary>
##	Execute dpkg programs in the dpkg domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`dpkg_run',`
	gen_require(`
		attribute_role dpkg_roles;
	')

	dpkg_domtrans($1)
	roleattribute $2 dpkg_roles;
')

########################################
## <summary>
##	Execute dpkg programs in the dpkg domain
##	from a NoNewPrivileges process.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dpkg_nnp_domtrans',`
	gen_require(`
		type dpkg_t;
	')

	allow $1 dpkg_t:process2 nnp_transition;
')

########################################
## <summary>
##	Execute the dkpg in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_exec',`
	gen_require(`
		type dpkg_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, dpkg_exec_t)
')

########################################
## <summary>
##	Execute the debsums programs
##	in the debsums domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dpkg_debsums_domtrans',`
	gen_require(`
		type debsums_t, debsums_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, debsums_exec_t, debsums_t)
')

########################################
## <summary>
##	Execute the debsums programs in
##	the debsums domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`dpkg_debsums_run',`
	gen_require(`
		type debsums_t;
	')

	dpkg_debsums_domtrans($1)
	role $2 types debsums_t;
')

########################################
## <summary>
##	Execute dpkg-preconfigure in the dpkg-preconfigure domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dpkg_preconfigure_domtrans',`
	gen_require(`
		type dpkg_preconfigure_t, dpkg_preconfigure_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, dpkg_preconfigure_exec_t, dpkg_preconfigure_t)
')

########################################
## <summary>
##	Execute dpkg-preconfigure in the dpkg-preconfigure domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`dpkg_preconfigure_run',`
	gen_require(`
		attribute_role dpkg_roles;
	')

	dpkg_preconfigure_domtrans($1)
	roleattribute $2 dpkg_roles;
')

########################################
## <summary>
##	Execute the update-alternatives programs
##	in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_update_alternatives_exec',`
	gen_require(`
		type update_alternatives_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, update_alternatives_exec_t)
')

########################################
## <summary>
##	Execute the update-alternatives programs
##	in the updates-alternatives domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`dpkg_update_alternatives_domtrans',`
	gen_require(`
		type update_alternatives_t, update_alternatives_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, update_alternatives_exec_t, update_alternatives_t)
')

########################################
## <summary>
##	Execute the update-alternatives programs in
##	the update-alternatives domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`dpkg_update_alternatives_run',`
	gen_require(`
		attribute_role update_alternatives_roles;
	')

	dpkg_update_alternatives_domtrans($1)
	roleattribute $2 update_alternatives_roles;
')

########################################
## <summary>
##	Get the attributes of the dpkg binary.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_getattr_exec',`
	gen_require(`
		type dpkg_exec_t;
	')

	corecmd_search_bin($1)
	allow $1 dpkg_exec_t:file getattr;
')

########################################
## <summary>
##	Inherit and use file descriptors from dpkg.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_use_fds',`
	gen_require(`
		type dpkg_t;
	')

	allow $1 dpkg_t:fd use;
')

########################################
## <summary>
##	Read and write unnamed dpkg pipes.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_rw_inherited_pipes',`
	gen_require(`
		type dpkg_t;
	')

	allow $1 dpkg_t:fifo_file rw_inherited_fifo_file_perms;
')

########################################
## <summary>
##	Inherit and use file descriptors
##	from dpkg scripts.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_use_script_fds',`
	gen_require(`
		type dpkg_script_t;
	')

	allow $1 dpkg_script_t:fd use;
')

########################################
## <summary>
##	Inherit and use file descriptors
##	from dpkg scripts.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_rw_inherited_script_pipes',`
	gen_require(`
		type dpkg_script_t;
	')

	allow $1 dpkg_script_t:fd use;
	allow $1 dpkg_script_t:fifo_file rw_inherited_fifo_file_perms;
')

########################################
## <summary>
##	Open pipes from dpkg scripts.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_open_script_pipes',`
	gen_require(`
		type dpkg_script_t;
	')

	allow $1 dpkg_script_t:fifo_file open;
')

########################################
## <summary>
##	Write to inherited files from dpkg scripts.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_write_inherited_script_files',`
	gen_require(`
		type dpkg_script_tmp_t;
	')

	allow $1 dpkg_script_tmp_t:file { getattr write };
	neverallow $1 dpkg_script_tmp_t:file open;
')

########################################
## <summary>
##	Search dpkg package database content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_search_db',`
	gen_require(`
		type dpkg_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir search_dir_perms;
')

########################################
## <summary>
##	Get the attributes of dpkg package database files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_getattr_db_files',`
	gen_require(`
		type dpkg_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir search_dir_perms;
	allow $1 dpkg_state_t:file getattr;
')

########################################
## <summary>
##	Read dpkg package database content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_read_db',`
	gen_require(`
		type dpkg_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir list_dir_perms;
	allow $1 dpkg_state_t:lnk_file read_lnk_file_perms;
	allow $1 dpkg_state_t:file mmap_read_file_perms;
')

########################################
## <summary>
##	Create, read, write, and delete
##	dpkg package database content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_manage_db',`
	gen_require(`
		type dpkg_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir rw_dir_perms;
	allow $1 dpkg_state_t:lnk_file manage_lnk_file_perms;
	allow $1 dpkg_state_t:file manage_file_perms;
')

########################################
## <summary>
##	Create, read, write, and delete
##	dpkg lock files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_lock_db',`
	gen_require(`
		type dpkg_lock_t, dpkg_state_t;
	')

	filetrans_pattern($1, dpkg_state_t, dpkg_lock_t, file, "lock")
	filetrans_pattern($1, dpkg_state_t, dpkg_lock_t, file, "lock-frontend")

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir list_dir_perms;
	allow $1 dpkg_lock_t:file manage_file_perms;
')

########################################
## <summary>
##	Read the dpkg trigger states.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_read_trigger_db',`
	gen_require(`
		type dpkg_state_t, dpkg_trigger_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir search_dir_perms;
	allow $1 dpkg_trigger_state_t:dir search_dir_perms;
	allow $1 dpkg_trigger_state_t:file read_file_perms;
')

########################################
## <summary>
##	Create, read, write, and delete
##	the dpkg trigger lock file.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_lock_trigger_db',`
	gen_require(`
		type dpkg_state_t, dpkg_trigger_lock_t, dpkg_trigger_state_t;
	')

	#filetrans_pattern($1, dpkg_trigger_state_t, dpkg_trigger_lock_t, file, "Lock")

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir search_dir_perms;
	allow $1 dpkg_trigger_state_t:dir list_dir_perms;
	allow $1 dpkg_trigger_lock_t:file rw_file_perms;
')

########################################
## <summary>
##	Create, read, write, and delete
##	dpkg trigger database content.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_manage_trigger_db',`
	gen_require(`
		type dpkg_trigger_state_t;
	')

	files_search_var_lib($1)
	dpkg_lock_trigger_db($1)
	allow $1 dpkg_trigger_state_t:dir rw_dir_perms;
	allow $1 dpkg_trigger_state_t:file manage_file_perms;
')

########################################
## <summary>
##	Read the dpkg config files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_read_config',`
	gen_require(`
		type dpkg_conf_t;
	')

	files_search_etc($1)
	allow $1 dpkg_conf_t:dir list_dir_perms;
	allow $1 dpkg_conf_t:lnk_file read_lnk_file_perms;
	allow $1 dpkg_conf_t:file read_file_perms;
')

########################################
## <summary>
##	Read the update-alternatives database files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`dpkg_read_alternatives_db',`
	gen_require(`
		type dpkg_alternatives_state_t, dpkg_state_t;
	')

	files_search_var_lib($1)
	allow $1 dpkg_state_t:dir search_dir_perms;
	allow $1 dpkg_alternatives_state_t:dir list_dir_perms;
	allow $1 dpkg_alternatives_state_t:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an dpkg environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`dpkg__admin',`
	dpkg_run($1, $2)
	dpkg_debsums_run($1, $2)
	dpkg_update_alternatives_run($1, $2)
')
