## <summary>On-line manual database.</summary>

########################################
## <summary>
##	Execute a domain transition to run man.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`mandb_domtrans_man',`
	gen_require(`
		type man_t, man_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, man_exec_t, man_t)
')

########################################
## <summary>
##	Execute man in the man domain,
##	and allow the specified role
##	the man domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mandb_run_man',`
	gen_require(`
		type man_t;
	')

	mandb_domtrans_man($1)
	role $2 types man_t;
')

########################################
## <summary>
##	Execute the mandb program in
##	the mandb domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`mandb_domtrans_mandb',`
	gen_require(`
		type mandb_t, mandb_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, mandb_exec_t, mandb_t)
')

########################################
## <summary>
##	Execute mandb in the mandb
##	domain, and allow the specified
##	role the mandb domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mandb_run_mandb',`
	gen_require(`
		attribute_role mandb_roles;
	')

	mandb_domtrans_mandb($1)
	roleattribute $2 mandb_roles;
')

########################################
## <summary>
##	Read the mandb configuration file (/etc/manpath.config).
##	Needed for the usage of man(1).
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`mandb_read_config',`
	gen_require(`
		type mandb_conf_t;
	')

	files_search_etc($1)
	allow $1 mandb_conf_t:file read_file_perms;
')

########################################
## <summary>
##	All of the rules required to
##	administrate an mandb environment.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`mandb__admin',`
	gen_require(`
		type mandb_t;
	')

	admin_process_pattern($1, mandb_t)

	mandb_run_man($1, $2)
	mandb_run_mandb($1, $2)
')
