## <summary>Load keyboard mappings.</summary>

########################################
## <summary>
##	Execute the loadkeys program in
##	the loadkeys domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`loadkeys_domtrans',`
	gen_require(`
		type loadkeys_t, loadkeys_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, loadkeys_exec_t, loadkeys_t)
')

########################################
## <summary>
##	Execute the loadkeys program in
##	the loadkeys domain, and allow the
##	specified role the loadkeys domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_run',`
	gen_require(`
		attribute_role loadkeys_roles;
	')

	loadkeys_domtrans($1)
	roleattribute $2 loadkeys_roles;
')

########################################
## <summary>
##	Execute the loadkeys program in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_exec',`
	gen_require(`
		type loadkeys_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, loadkeys_exec_t)
')

########################################
## <summary>
##	Execute loadkeys setup programs in
##	the loadkeys setup domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`loadkeys_domtrans_setup',`
	gen_require(`
		type loadkeys_setup_t, loadkeys_setup_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, loadkeys_setup_exec_t, loadkeys_setup_t)
')

########################################
## <summary>
##	Execute loadkeys setup programs in
##	the loadkeys setup domain, and allow the
##	specified role the loadkeys setup domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_run_setup',`
	gen_require(`
		attribute_role loadkeys_roles;
	')

	loadkeys_domtrans_setup($1)
	roleattribute $2 loadkeys_roles;
')

########################################
## <summary>
##	Execute the loadkeys setup programs in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_exec_setup',`
	gen_require(`
		type loadkeys_setup_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, loadkeys_setup_exec_t)
')

########################################
## <summary>
##	Read the loadkeys console configurations.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_read_config',`
	gen_require(`
		type loadkeys_conf_t;
	')

	files_search_etc($1)
	allow $1 loadkeys_conf_t:dir list_dir_perms;
	allow $1 loadkeys_conf_t:file read_file_perms;
')

########################################
## <summary>
##	Execute the console script in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_exec_scripts',`
	gen_require(`
		type loadkeys_script_t;
	')

	can_exec($1, loadkeys_script_t)
')

########################################
## <summary>
##	Initialize the console-setup runtime directory.
##	Includes manual creation for rescue mode.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`loadkeys_init_runtime_entries',`
	gen_require(`
		type loadkeys_runtime_t;
	')

	files_runtime_filetrans($1, loadkeys_runtime_t, dir, "console-setup")
	allow $1 loadkeys_runtime_t:dir { add_entry_dir_perms create_dir_perms };
	allow $1 loadkeys_runtime_t:file { create_file_perms write };
')

########################################
## <summary>
##	Reverse interface for systemd.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_t).
##	</summary>
## </param>
## <param name="domain">
##	<summary>
##	Domain allowed access (systemd_generic_bin_t).
##	</summary>
## </param>
#
interface(`loadkeys__systemd',`
	gen_require(`
		type loadkeys_runtime_t;
	')

	allow $1 loadkeys_runtime_t:dir { read relabelto search };
	allow $1 loadkeys_runtime_t:file relabelto;
')
