#
# Common domain transition pattern perms
#
# Parameters:
# 1. source domain
# 2. entry point file type
# 3. target domain
#
define(`domain_transition_pattern',`
	allow $1 $2:file { getattr open read execute };
	allow $1 $3:process transition;
	dontaudit $1 $3:process { noatsecure siginh rlimitinh };
')

#
# Specified domain transition patterns
#
# Parameters:
# 1. source domain
# 2. entry point file type
# 3. target domain
#
define(`spec_domtrans_pattern',`
	allow $1 self:process setexec;
	domain_transition_pattern($1,$2,$3)

	allow $3 $1:fd use;
	allow $3 $1:fifo_file rw_inherited_fifo_file_perms;
	allow $3 $1:process sigchld;
')

#
# Automatic domain transition patterns
#
# Parameters:
# 1. source domain
# 2. entry point file type
# 3. target domain
#
define(`domain_auto_transition_pattern',`
	domain_transition_pattern($1,$2,$3)
	type_transition $1 $2:process $3;
')

#
# Automatic domain transition patterns
# with feedback permissions
#
# Parameters:
# 1. source domain
# 2. entry point file type
# 3. target domain
#
define(`domtrans_pattern',`
	domain_auto_transition_pattern($1,$2,$3)

	allow $3 $1:fd use;
	allow $3 $1:fifo_file rw_inherited_fifo_file_perms;
	allow $3 $1:process sigchld;
')

#
# Read foreign domain proc data
#
# Parameters:
# 1. source domain
# 2. target domain
# TODO: use more
define(`read_process_pattern',`
	allow $1 $2:dir search_dir_perms;
	allow $1 $2:file { getattr ioctl open read };
	allow $1 $2:lnk_file read;
')

#
# Read foreign domain proc data
#
# Parameters:
# 1. source domain
# 2. target domain
#
define(`ps_process_pattern',`
	allow $1 $2:dir list_dir_perms;
	allow $1 $2:file read_file_perms;
	allow $1 $2:lnk_file read_lnk_file_perms;
	# read security attribute
	allow $1 $2:process getattr;
')

#
# Process user access pattern
#
# Parameters:
# 1. source domain
# 2. target domain
#
define(`user_process_pattern',`
	ps_process_pattern($1, $2)

	allow $1 $2:process signal_perms;
')

#
# Process administration pattern
#
# Parameters:
# 1. source domain
# 2. target domain
#
define(`admin_process_pattern',`
	user_process_pattern($1, $2)

	tunable_policy(`allow_ptrace',`
		allow $1 $2:process ptrace;
	')
')

#
# Service administration pattern
#
# Parameters:
# 1. source domain
# 2. target domain
# 3. target script file domain
# 4. target unit file domain
define(`admin_service_pattern',`
	admin_process_pattern($1, $2)

	systemd_startstop_service($1, $4)

	allow $1 $3:file read_file_perms;
	allow $1 $4:file read_file_perms;
')

#
# Neverallow a direct transition
#
# Parameters:
# 1. source domain
# 2. target domain
define(`neverallow_transition_pattern',`
	neverallow $1 $2:process { transition dyntransition noatsecure siginh rlimitinh };
	neverallow $1 $2:process2 { nnp_transition nosuid_transition };
')

#
# File execution pattern
#
# Parameters:
# 1. source domain
# 2. executable file type
#
define(`can_exec',`allow $1 $2:file { mmap_exec_file_perms ioctl lock execute_no_trans };')

#
# Check for DAC execute permission, e.g. via access(2)
#
# Parameters:
# 1. source domain
# 2. executable file type
#
define(`can_check_exec',`
	allow $1 $2:file { execute getattr };
	neverallow $1 $2:file { entrypoint execute_no_trans };
')

#
# Check for DAC write permission on directories, e.g. via access(2)
#
# Parameters:
# 1. source domain
# 2. dir type
#
define(`can_check_write_dir',`
	allow $1 $2:dir { getattr write };
	neverallow $1 $2:dir { add_name remove_name };
')
